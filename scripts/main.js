/**
 * Variables globals
 */
var g = {

    /**
     * Any amb el que s'inicia la infografia
     */
    anyActual: '2006',

    /**
     * Nombre totals d'anys que hi ha
     */
    numAnys:   $('div[role="main"] > div').length,

    /**
     * % de moviment extra que tindrà per donar l'efecte de retrocès
     */
    movExtraFX: 10,

    /**
     * El temps que triga a fer-se l'animació
     */
    tempsImatgeBG: 1000
},

    /**
     * ID's de los audios para escuchar de youtube
     */
    youtubeAudiosIDs = {
        2006: {
            'id': 'ZzwvItK3JPI',
            'params': ''
        },
        2007: {
            'id': 'zpYSxEPz828',
            'params': ''
        },
        2008: {
            'id': 'GpACca8cR8E',
            'params': ''
        },
        2009: {
            'id': 'VTjNAxeh74k',
            'params': ''
        },
        2010: {
            'id': 'bE7XVg594_o',
            'params': ''
        },
        2011: {
            'id': 'khFnevT4olc',
            'params': ''
        },
        2012: {
            'id': 'SAzzbAIRT08',
            'params': ''
        },
        2013: {
            'id': 'r4yMPRQwta4',
            'params': ''
        }
    },

    videoAction = 'hide';


/**
 * Modificar linia del temps
 */
$('.footer li a').click(function(){

    var anyActual = parseInt($(this).attr('data-any'), 10);

    // Movem cap l'any clicat
    var carALaDreta = anyActual > parseInt(g.anyActual, 10),
        vegades     = Math.abs(anyActual - parseInt(g.anyActual, 10)),
        percentatjeMoure = 100  * vegades;

    percentatjeMoure  = carALaDreta ? '-=' + (percentatjeMoure + g.movExtraFX) : '+=' + (percentatjeMoure + g.movExtraFX);
    percentatjeRestar = carALaDreta ? '+=' + g.movExtraFX                      : '-=' + g.movExtraFX;

    console.log(percentatjeRestar);

    // Si tenia una animació la parem
    // $('div[role="main"]').stop();

    $('div[role="main"]').animate({
        marginLeft: percentatjeMoure + '%'
    }, {duration: g.tempsImatgeBG * vegades, easing: 'easeInSine'}).animate({
        marginLeft: percentatjeRestar + '%'
    }, {duration: 400, easing: 'easeOutCubic'});

    // Modificamos el auido de fondo
    // Eliminamos el anterior
    $('#youtubeAudio').remove();
    console.log(anyActual);
    // Creamos el nuevo
    $('<iframe>').attr({
        src: 'https://www.youtube.com/embed/' + youtubeAudiosIDs[anyActual].id + '?autoplay=1&loop=1&rel=0&wmode=transparent' + youtubeAudiosIDs[anyActual].params,
        id: 'youtubeAudio',
        width: 300,
        height: 300
    }).appendTo('body');

    // Añadimos la clase activo al año actual
    $('.active').removeClass('active');

    $('[data-any="' + anyActual + '"]').addClass('active');

    $('.playPausaVideo').attr({ src: 'images/sound_on.png' });

    g.anyActual = anyActual;
});


$('.datos').click(function(){
    toggleBarra(this);
});

$('.playPausaVideo').click(function(){
    toggleVideo();

    $(this).attr({
        src: 'images/sound_' + $(this).attr('data-mode') + '.png'
    });

    $(this).attr('data-mode', ($(this).attr('data-mode') == 'on') ? 'off' : 'on');
});


/* FUNCIONES
--------------------- */
function toggleBarra(target)
{
    var varMarginLeft = parseInt($(target).css('margin-left').replace('px', ''), 10),
        margin = (varMarginLeft < 0) ? '0px' : '-' + $(target).width() + 'px';

    $(target).animate({ 'margin-left': margin });
}

function autoPlayVideo(vcode, width, height){
    "use strict";
    $("#videoContainer").html('<iframe width="'+width+'" height="'+height+'" src="https://www.youtube.com/embed/'+vcode+'?autoplay=1&loop=1&rel=0&wmode=transparent" frameborder="0" allowfullscreen wmode="Opaque"></iframe>');
}

function toggleVideo()
{
    if (videoAction == 'hide') {
        $('#youtubeAudio').remove();
        videoAction = 'none';
    } else {
        $('<iframe>').attr({
            src: 'https://www.youtube.com/embed/' + youtubeAudiosIDs[g.anyActual].id + '?autoplay=1&loop=1&rel=0&wmode=transparent' + youtubeAudiosIDs[g.anyActual].params,
            id: 'youtubeAudio',
            width: 300,
            height: 300
        }).appendTo('body');
        videoAction = 'hide';
    }



    //iframe.postMessage('{"event":"command","func":"' + videoAction + '","args":""}', '*');
    //console.log(videoAction);


}